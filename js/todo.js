//Autre exercice
let count = 0;
//Fonction quand on clique barre les éléments et affiche en vert

$("#ul").on("click", "li", function () {
  $(this).toggleClass("completed");
});

//Fonction pour faire disparaitre les éléments

$("#ul").on("click", "button", function (event) {
  $(this)
    .parent()
    .fadeOut(700, function () {
      $(this).remove();
    });
  event.stopPropagation();
});

//Afficher la valeur rentré dans l'input en appuyant sur entrée

$(document).keypress((event) => {
  let str = $("#input").val();
  if (str != "") {
    if (event.keyCode == "13" || event.which == "13") {
      $("ul").append(
        `<li class="new"><button class="supprimer">X </button> Tâche ${(count += 1)} : ${str}</li>`
      );
      $("#input").val("");
    }
  }
});

//Quand on clique sur le bouton valider alors cela valide notre input
$("#btn1").click(function () {
  let string = $("#input").val();
  $("ul").append(
    `<li class="new"><button class="supprimer"><i class="fas fa-trash"></i> </button> Tâche ${(count += 1)} : ${string}</li>`
  );
  $("#input").val("");
});
